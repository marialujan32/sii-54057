// Raqueta.cpp: implementation of the Raqueta class.
//María Luján
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	tamano=5;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
//movimiento en x
	x1=x1+velocidad.x*t;
	x2=x2+velocidad.x*t;
//movimiento en y 
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
}

bool Raqueta::recibeDisparo(Plano& p){
//Intersección esfera con plano
	Esfera e;
	e.centro.x=(p.x1+p.x2)/2;
	e.centro.y=(p.y1+p.y2)/2;
	e.radio=0.5*sqrt((p.x1-p.x2)*(p.x1-p.x2)+(p.y1-p.y2)*(p.y1-p.y2));
//Devuelve 1 si rebota
	if(Plano::Rebota(e))
	{
		return true;
	}
	return false;
}

void Raqueta::agrandar(int n){
	if(n==-1){
		if(y1<y2){
			if(tamano>0){
				y1+=0.1f;
				y2-=0.1f;
				tamano--;
			}
		}
		else{
			if(tamano>0){
				y1-=0.1f;
				y2+=0.1f;
				tamano--;
			}
		}

	}
	else if(n==1){
		if(y1<y2){
			if(tamano<5){
				y1-=0.1f;
				y2+=0.1f;
				tamano++;
			}
		}
		else{
			if(tamano<5){
				y1+=0.1f;
				y2-=0.1f;
				tamano++;
			}
		}

	}

}

